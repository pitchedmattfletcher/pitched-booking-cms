<?php 
    /**
     * Register admin scripts and styles
     */
    add_action('admin_enqueue_scripts', function ($hook) {
        wp_enqueue_script('main-admin', get_template_directory_uri() . '/js/main.js', array('jquery'), wp_get_theme()->get('Version'), true);
    });

?>