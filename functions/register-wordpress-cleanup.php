<?php

/**
 * Remove comments from posts and pages
 */
add_action('init', function () {
    remove_post_type_support('post', 'comments');
    remove_post_type_support('page', 'comments');
    remove_post_type_support('post', 'post');
}, 100);


/**
 * Remove comments from admin menu
 */
add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
    remove_menu_page('edit.php');
});

/**
 * Remove comments from admin bar
 */
add_action('wp_before_admin_bar_render', function () {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
});

/**
 * Remove new post button from admin bar
 */
add_action( 'admin_bar_menu', function ($wp_admin_bar) {
    $wp_admin_bar->remove_node('new-post');
}, 999);

/**
 * Remove new post draft from dashboard
 */
add_action( 'wp_dashboard_setup', function () {
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
}, 999);


/*
 * Stop admin bar from adding 32px margin-top to html
 */
add_action('get_header', function () {
    remove_action('wp_head', '_admin_bar_bump_cb');
});




/** 
 * Move YOAST SEO to bottom
 */
add_filter( 'wpseo_metabox_prio', function () {
	return 'low';
});


// Disable REST API link tag
remove_action('wp_head', 'rest_output_link_wp_head', 10);

// Disable oEmbed Discovery Links
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);

// Disable REST API link in HTTP headers
remove_action('template_redirect', 'rest_output_link_header', 11, 0);

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');

/**
 * Remove block css
 */
add_action('wp_print_styles', function () {
    if (!is_admin()) {
        wp_dequeue_style( 'wp-block-library' );
    }
}, 100);

/**
 * Remove Jquery Migrate
 */
add_action('wp_default_scripts', function ($scripts) {
    if (!is_admin() && !empty( $scripts->registered['jquery'])) {
        $scripts->registered['jquery']->deps = array_diff(
            $scripts->registered['jquery']->deps,
            ['jquery-migrate']
        );
    }
});

/**
 * Disable wp-embed
 */
add_action('init', function () {
    remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
    remove_filter('pre_oembed_result', 'wp_filter_pre_oembed_result', 10);
    remove_action('rest_api_init', 'wp_oembed_register_route');
    remove_action('wp_head', 'wp_oembed_add_discovery_links');
    remove_action('wp_head', 'wp_oembed_add_host_js');
    add_filter( 'tiny_mce_plugins', function ($plugins) {
        return array_diff($plugins, array('wpembed'));
    });
    add_filter( 'rewrite_rules_array', function ($rules) {
        foreach($rules as $rule => $rewrite) {
            if(false !== strpos($rewrite, 'embed=true')) {
                unset($rules[$rule]);
            }
        }
        return $rules;
    });
    add_filter('embed_oembed_discover', '__return_false');
    add_filter( 'emoji_svg_url', '__return_false' );

}, 9999);
   
/**
 * Disable wp emoji's
 */
add_action('init', function () {
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles'); 
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji'); 
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    add_filter('tiny_mce_plugins', function ($plugins) {
        if (is_array( $plugins)) {
            return array_diff($plugins, array('wpemoji'));
        } else {
            return array();
        }
    });
    add_filter('wp_resource_hints', function ($urls, $relation_type) {
        if ('dns-prefetch' == $relation_type) {
            $emoji_svg_url = apply_filters('emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/');
            $urls = array_diff( $urls, array( $emoji_svg_url ) );
        }
        return $urls;
    }, 10, 2);
});

/**
 * Remove feed scripts
 */
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'feed_links', 2);


remove_image_size('1536x1536');
remove_image_size('2048x2048');

//set_post_thumbnail_size(200, 200, true);



/**
 * Allow access for SVGs
 */
add_action('upload_mimes', function ($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types; 
});

?>