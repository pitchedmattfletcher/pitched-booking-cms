<?php 

// Set API prefix
add_filter('rest_url_prefix', function() {
    return 'api';
});


/**
 * Modify REST API responses
 */
function prepare_post($response) {    

    // Generate SEO data
    if ($response->data['type'] === 'page') { 
        $response->data['seo'] = array(
            'title' => $response->data['yoast_head_json']['og_title'],
            'description' => $response->data['yoast_head_json']['description'],
            'robots' => array (
                'index' => $response->data['yoast_head_json']['robots']['index'],
                'follow' => $response->data['yoast_head_json']['robots']['follow'],
            ),
        );
    }

    // Modify 
    $response->data['title'] = $response->data['title']['rendered'];
    $response->data['content'] = $response->data['content']['rendered'];

    // Get custom fields
    $response->data['acf'] = get_custom_fields($response->data['id']);

    // Remove links
    $response->remove_link('self');
    $response->remove_link('collection');
    $response->remove_link('about');
    $response->remove_link('author');
    $response->remove_link('replies');
    $response->remove_link('version-history');
    $response->remove_link('predecessor-version');
    $response->remove_link('curies');
    $response->remove_link('wp:attachment');
    $response->remove_link( 'https://api.w.org/featuredmedia' );
    $response->remove_link( 'https://api.w.org/attachment' );
    $response->remove_link( 'https://api.w.org/term' );
    
    // Remove uncessesary data
    unset($response->data['date']);
    unset($response->data['date_gmt']);
    unset($response->data['guid']);
    unset($response->data['modified']);
    unset($response->data['modified_gmt']);
    unset($response->data['link']);
    unset($response->data['excerpt']);
    unset($response->data['author']);
    unset($response->data['featured_media']);
    unset($response->data['parent']);
    unset($response->data['menu_order']);
    unset($response->data['comment_status']);
    unset($response->data['ping_status']);
    unset($response->data['yoast_head']);
    unset($response->data['yoast_head_json']);
    unset($response->data['meta']);

    return $response;

}
    
// Modify page/post responses
add_filter('rest_prepare_page', 'prepare_post');  
add_filter('rest_prepare_post', 'prepare_post');  
add_filter('rest_prepare_testimonial', 'prepare_post');  

/**
 * Retrieve all custom fields for a specific post
 */
function get_custom_fields($post_id) {
    $custom_fields = new stdClass();
    $fieldObjects = get_field_objects($post_id, false, true);
    if ($fieldObjects) {
        foreach ($fieldObjects as $fieldObject) {
            $field_value = $fieldObject['value'];
            $field_name = $fieldObject['name'];
            
            // If is true/false 
            if ($fieldObject['type'] === 'true_false') {
                $field_value = $field_value === '1' ? true : false;
            }

            if ($fieldObject['type'] === 'image') {
                $field_value = wp_get_attachment_image_src($field_value, 'full')[0];
            }

            // Handle flexible content a bit differently...
            if ($fieldObject['type'] === 'flexible_content') {
                $field_value = get_field($fieldObject['name'], $post_id);
                $field_value = $field_value ? $field_value : array();

                foreach ($field_value as &$layout_item) {
                    $layout_item['flexible_content_name'] = $layout_item['acf_fc_layout'];
                    unset($layout_item['acf_fc_layout']);
                }
            }

            // Set field name to field value
            $custom_fields->$field_name = $field_value;
        }
    }
    return $custom_fields;
}

/**
 * Call the API
 */
function call_api($path) {
    $request = new WP_REST_Request('GET', $path);
    $response = rest_do_request($request);
    $server = rest_get_server();
    $data = $server->response_to_data($response, false);
    return $data;
}

/**
 * Populate testimonials
 */
add_filter('acf/load_field/name=testimonials', function ($field) {
    $testimonials = new WP_Query(array(   
        'post_type' => 'testimonial',
        'post_status' => 'publish',
        'orderby' => 'ASC',
        'posts_per_page' => -99
    ));
    $choices = [];
    if ($testimonials->have_posts()) {
        while ($testimonials->have_posts()) { $testimonials->the_post();
            $choices[get_the_ID()] = get_the_title(get_the_ID());
        }
    }
    wp_reset_query();
    $field['choices'] = $choices;
	return $field;
});


/**
 * Return testimonials
 */
add_filter('acf/format_value/name=testimonials', function ($value, $post_id, $field) {
    $result = array();
    foreach ($value as $testimonial_id) {
        array_push($result, call_api('/wp/v2/testimonials/' . $testimonial_id));
    }
    return $result;
}, 10, 3);


?>