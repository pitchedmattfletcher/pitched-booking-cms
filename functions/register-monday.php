<?php 

// Register monday
add_action( 'rest_api_init', function () {
    
    // Create monday contact
    register_rest_route('/monday/v1', 'create-contact', array(
        'methods' => 'POST',
        'callback' => function($request){
            $json = $request->get_json_params();
            $query = 'mutation ($myItemName: String!, $columnVals: JSON!) { create_item (board_id:2840579482, item_name:$myItemName, column_values:$columnVals) { id } }';
            $vars = json_encode(array(
                'myItemName' => $json['companyName'],
                'columnVals' => json_encode(array(
                    'status' => $json['status'] ? $json['status'] : 'Booking Sign-up',
                    'text' => $json['firstName'],
                    'text09' => $json['lastName'],
                    'phone' => array('phone' => $json['phone'], 'text' => $json['phone'], 'countryShortName' => 'GB'),
                    'email' => array('email' => $json['email'], 'text' => $json['email']),
                    'link' => array('url' => $json['companyWebsite'], 'text' => $json['companyWebsite']),
                    'text5' => $json['addressLine1'],
                    'text1' => $json['addressLine2'],
                    'text93' => $json['city'],
                    'text3' => $json['county'],
                    'text4' => $json['postcode'],
                    'long_text' => $json['notes'],
                ))
            ));

            // Post to monday
            $request = wp_remote_post('https://api.monday.com/v2', array(
                'method' => 'POST',
                'data_format' => 'body',
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Authorization' => MONDAY_TOKEN,
                ),
                'body' => json_encode(array(
                    'query' => $query,
                    'variables' => $vars,
                )),
            ));
            
            // Error handling
            if(is_wp_error($request)) {
                return array('success' => false);
            } else {
                return array('success' => true);
            }
        },
    ));
});
?>