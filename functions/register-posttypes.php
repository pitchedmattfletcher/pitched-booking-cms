<?php 

// Testimonials
add_action('init', function () {
    register_post_type('testimonial', array(
        'labels'             => array(
            'name'               => _x( 'Testimonial', 'post type general name'),
            'singular_name'      => _x( 'Testimonial', 'post type singular name'),
            'menu_name'          => _x( 'Testimonial', 'admin menu'),
            'name_admin_bar'     => _x( 'Testimonial', 'Add New'),
            'add_new'            => _x( 'Add New', 'Testimonial'),
            'add_new_item'       => __( 'Add New Testimonial'),
            'new_item'           => __( 'New Testimonial' ),
            'edit_item'          => __( 'Edit Testimonial'),
            'view_item'          => __( 'View Testimonial'),
            'all_items'          => __( 'All Testimonials'),
            'search_items'       => __( 'Search Testimonials'),
            'parent_item_colon'  => __( 'Parent Testimonial:'),
            'not_found'          => __( 'No Testimonial found.'),
            'not_found_in_trash' => __( 'No Testimonial found in Trash.')
        ),
        'public'             => true,
        'menu_icon'          => 'dashicons-marker',
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'rest_base'          => 'testimonials',
        'query_var'          => true,
        'has_archive'        => false,
        'hierarchical'       => true,
        'menu_position'      => 20,
        'supports'           => array('title', 'thumbnail', 'editor')
    ));
}, 0);
?>