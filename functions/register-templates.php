<?php 
add_filter('theme_page_templates', function($page_templates, $wp_theme, $post) {

    // My custom templates
    $custom_page_templates = array(
        //'template_accommodation_listing' => 'Template: Accommodation Listing',
        'template_front_page' => 'Template: Front Page',
    );

    // Register custom templates
    foreach ($custom_page_templates as $slug => $name) {
        if(!isset( $page_templates[$slug]))
            $page_templates[$slug] = $name;
    }

    // Return custom templtes
    return $page_templates;

}, PHP_INT_MAX, 3);


add_action( 'admin_init', function () {
    remove_post_type_support( 'page', 'editor' );
});
 


?>