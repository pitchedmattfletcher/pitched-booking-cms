<?php
    include (get_template_directory() . '/functions/register-acf.php');
    include (get_template_directory() . '/functions/register-api.php');
    include (get_template_directory() . '/functions/register-monday.php');
    include (get_template_directory() . '/functions/register-posttypes.php');
    include (get_template_directory() . '/functions/register-wordpress-cleanup.php');
    include (get_template_directory() . '/functions/register-templates.php');
    include (get_template_directory() . '/functions/register-scripts.php');

?>