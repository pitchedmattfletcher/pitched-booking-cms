(function ($) {


    $(document).ready(function () {

        // Only do on dev
        if (window.location.origin === 'http://pichedbooking.local' || window.location.origin === 'https://pichedbooking.local') {

            // If ACF exists on current page...
            if (typeof acf !== 'undefined') { 

                // On ACF load field. Add a button that allows devs to copy the field name.
                acf.addAction('load_field', function (field) {
                    if (field.data.type === 'message' || field.data.type === 'accordion') {
                        return;
                    }
                    field.$el.find('.acf-label label:first').append('<p class="acf-api-copy" style="display:inline;padding:2px 4px;background-color:#f1f1f1;margin-left:5px;border-radius:5px;font-weight:400;border:1px solid #e1e1e1">' + field.data.name + '</p>');
                });  

                
                // On clicking any of the above buttons, 
                // copy the fieldname to the clickboard.
                $(document).on('click', '.acf-api-copy', function () {
                    var el = $(this);
                    var aux = document.createElement("input");
                    aux.setAttribute("value", el.text());
                    document.body.appendChild(aux);
                    aux.select();
                    document.execCommand("copy");
                    document.body.removeChild(aux);
                });

            }
        }

    });

})(jQuery);
